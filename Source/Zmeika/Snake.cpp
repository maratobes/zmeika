// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElBase.h"
#include "Interactable.h"
// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	Last = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
    SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnake::AddSnakeElement(int ElementsNum)
{
    for(int i=0; i<ElementsNum; ++i){
        //FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
        ASnakeElBase* NewSnakeEl = GetWorld()->SpawnActor<ASnakeElBase>(SnakeElementClass, FTransform(FVector(0,0,-100)));
        NewSnakeEl->SnakeOwner = this;
        int32 ElIndex = SnakeElements.Add(NewSnakeEl);
        if(ElIndex==0){
            NewSnakeEl->SetFirstEl();
            NewSnakeEl->SetActorLocation(FVector(0,0,0));
        }
    }
}
void ASnake::Move()
{
    FVector MovementVector;
    switch (Last)
    {
        case EMovementDirection::UP:
            MovementVector.X += ElementSize;
            break;
        case EMovementDirection::DOWN:
            MovementVector.X -= ElementSize;
            break;
        case EMovementDirection::LEFT:
            MovementVector.Y += ElementSize;
            break;
        case EMovementDirection::RIGHT:
            MovementVector.Y -= ElementSize;
            break;
    }
    SnakeElements[0]->ToggleCollision();
    for(int i = SnakeElements.Num() - 1; i>0; i--){
        auto CurrentEl = SnakeElements[i];
        auto PrevEl = SnakeElements[i-1];
        FVector PrevLoc = PrevEl->GetActorLocation();
        CurrentEl->SetActorLocation(PrevLoc);
    }
    SnakeElements[0]->AddActorWorldOffset(MovementVector);
    SnakeElements[0]->ToggleCollision();
}

void ASnake::SnakeElementOverlap(ASnakeElBase* OverlappedElement, AActor* Other){
    if(IsValid(OverlappedElement)){
        int32 Index;
        SnakeElements.Find(OverlappedElement, Index);
        bool IsFirst = Index==0;
        IInteractable* IntInt = Cast<IInteractable>(Other);
        if(IntInt){
            IntInt->Interact(this, IsFirst);
        }
    }
}