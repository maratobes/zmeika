// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Snake.h"
#include <stdlib.h>

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool IsHead){
    if(IsHead){
        auto snake = Cast<ASnake>(Interactor);
        if(IsValid(snake)){
            snake->AddSnakeElement();
            //FVector NewLoc = new FVector(0,0,0);
            this->SetActorLocation(FVector(rand()%900-450, rand()%900-450, 20));
            //this->Destroy();
        }
    }
}