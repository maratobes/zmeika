// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Snake.h"
// Sets default values
ASnakeElBase::ASnakeElBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElBase::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASnakeElBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElBase::SetFirstEl_Implementation(){
    MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElBase::HandleBeginOverlap);
}

void ASnakeElBase::Interact(AActor* Interactor, bool IsHead){
    auto snake = Cast<ASnake>(Interactor);
    if(IsValid(snake)){
        snake->Destroy();
    }
}
void ASnakeElBase::HandleBeginOverlap(
        UPrimitiveComponent* OverlappedComponent,
        AActor* OtherActor,
        UPrimitiveComponent* OtherComp,
        int32 OtherBodyIndex,
        bool bFromSweep,
        const FHitResult &SweepResult){
            if(IsValid(SnakeOwner)){
                SnakeOwner->SnakeElementOverlap(this, OtherActor);
            }
}
void ASnakeElBase::ToggleCollision(){
    if(MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
        MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    else
        MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

