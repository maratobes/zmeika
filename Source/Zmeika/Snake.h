// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snake.generated.h"

class ASnakeElBase;

UENUM()
enum class EMovementDirection
{
    UP,
    DOWN,
    LEFT,
    RIGHT
};
UCLASS()
class ZMEIKA_API ASnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnake();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElBase> SnakeElementClass;

	UPROPERTY()
	TArray<ASnakeElBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY()
	EMovementDirection Last;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum=1);
    UFUNCTION(BlueprintCallable)
	void Move();
    UFUNCTION()
    void SnakeElementOverlap(ASnakeElBase* OverlappedElement, AActor* Other);
};
